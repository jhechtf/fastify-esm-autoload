# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.2.2](https://gitlab.com/jhechtf/fastify-esm-autoload/compare/v1.2.1...v1.2.2) (2020-08-01)

### [1.2.1](https://gitlab.com/jhechtf/fastify-esm-autoload/compare/v1.2.0...v1.2.1) (2020-03-17)

## [1.2.0](https://gitlab.com/jhechtf/fastify-esm-autoload/compare/v1.1.0...v1.2.0) (2020-03-16)


### Features

* Added in auto-folder prefixing ([f607cf5](https://gitlab.com/jhechtf/fastify-esm-autoload/commit/f607cf5014972eb6fc18688c196fc2e9ae7a5e44)), closes [#5](https://gitlab.com/jhechtf/fastify-esm-autoload/issues/5)

## [1.1.0](https://gitlab.com/jhechtf/fastify-esm-autoload/compare/v1.0.6...v1.1.0) (2020-03-13)


### Features

* Added autoPrefix support similar to fastify-autoload ([68a1e52](https://gitlab.com/jhechtf/fastify-esm-autoload/commit/68a1e5260c64cf105fe4039114fcd26ca9fcbdde))

### 1.0.6 (2020-03-13)


### Bug Fixes

* Fixed plugin throwing errors on empty directories ([eb6c568](https://gitlab.com/jhechtf/fastify-esm-autoload/commit/eb6c568c53e765d0ed09f9c748f21e971df79972)), closes [#1](https://gitlab.com/jhechtf/fastify-esm-autoload/issues/1)

### 1.0.5 (2020-03-08)


### Bug Fixes

* Fixed plugin throwing errors on empty directories ([eb6c568](https://gitlab.com/jhechtf/fastify-esm-autoload/commit/eb6c568c53e765d0ed09f9c748f21e971df79972)), closes [#1](https://gitlab.com/jhechtf/fastify-esm-autoload/issues/1)

### [1.0.4](https://gitlab.com/jhechtf/fastify-esm-autoload/compare/v1.0.3...v1.0.4) (2020-03-05)

### [1.0.3](https://gitlab.com/jhechtf/fastify-esm-autoload/compare/v1.0.2...v1.0.3) (2020-03-05)

### [1.0.2](https://gitlab.com/jhechtf/fastify-esm-autoload/compare/v1.0.1...v1.0.2) (2020-03-05)

### 1.0.1 (2020-03-05)

## 1.0.0 (2020-03-03)
