# Fastify Autoload ES Modules


<span style="font-size: 1.5em;">**NOTE** The [fastify-autoload](https://github.com/fastify/fastify-autoload) plugin automatically works with ESM in Node as of [Version 3](https://github.com/fastify/fastify-autoload/tree/v3.0.0). If you are using this module and upgrade to Fastify 3, replace this module with the official Fastify module.</span>

I wrote this because I want to be able to use ES Modules (ESM) with Fastify and Node 12+. It has been tested to work in a Node 14.x environment as of 2020-05-01. Once Node 12.x is no longer in LTS, and if the default Fastify Autoload module has not yet added in this functionality, I shall remove any `--experimental-modules` flags from the project.

This is not meant to be an ESM Compatible fork of [fastify-autoload](https://github.com/fastify/fastify-autoload), as I don't have a lot of free time to map all of the use cases out.

Things this Fastify plugin does:

1. Allows you to have an autoloader with ESM

Things this plugin does not do:

1. Work with TypeScript -- at least in my quick and dirty tests of it. Perhaps I'll add the ability in once I learn more about the interactions of TypeScript with ESM in a Node environment.

## Installation

```
yarn add fastify-esm-autoload
# or npm install --save fastify-esm-autoload
```

## Usage

You must use this from within a Node ESM module. This means either using the `.mjs` extension, or setting `"type":"module"` on the top level of your `package.json` file.

```js
// server.js
import { resolve } from 'path';

import Fastify from 'fastify';
import Autoload from 'fastify-esm-autoload';

const fastify = Fastify();

fastify.register(Autload, {
  dir: resolve(process.cwd(), 'routes');
});

fastify.listen(3000, (err)=> {
  if(err) {
    console.error(err);
    process.exit();
  }
  console.log('Listening')
});

// routes/main.js
export default async (fastify) => {
  fastify.get('/', async () => {
    return 'working!';
  });
  return true;
}
```

Then, in your console run 

```sh
node --experimental-modules server.js
```

**NOTE**

In Node's ESM there is not the `__dirname` constant, so if your code relies on that, a simple fix is to do the following:

```js
import { resolve } from 'path';
const __dirname = resolve('.');
// or you could just use this code directly.

fastify.register(Autoload, {
  dir: resolve('.','routes')
})
```

Because you may want to pass options to a specific group of autoloaded plugins, you can do so like this:

```js
// server.js
import {resolve} from 'path';

import Fastify from 'fastify';
import Autoload from 'fastify-esm-autoload';

const fastify = Fastify();

fastify.register(Autoload, {
  dir: resolve(process.cwd(), 'grouped'),
  options: {
    something: 'some value '
  }
});
```

## Auto Prefix

If you would like to apply a prefix to all routes in a file, you can do this by exporting a constant called `autoPrefix`. 

```js
//server.js
import {resolve} from 'path';
import Fastify from 'fastify';
import Autoload from 'fastify-esm-autoload';
const fastify = Fastify();

fastify.register(Autoload, {
  dir: resolve(process.cwd(), 'routes')
});

//routes/posts.js
export default async (fastify) => {

  // will be registered under /posts
  fastify.get('/', async () => 'Post Index');
  // will be registered under /posts/:id
  fastify.get('/:id', async (req) => 'Post with ID of '+ req.params.id);
  // other routes...
  return true;
};
export const autoPrefix = '/posts';
```

**NOTE** My auto prefix is _not_ the same as the official Fastify Autoload. My auto prefix implementation will ***override*** any prefix given in the options object when the plugin is loaded. For example:

```js
// same imports as above example
fastify.register(Autoload, {
  dir: resolve(process.cwd(), 'routes'),
  prefix: '/defaultPrefix'
});

// routes/user.js
export default async (fastify) => {
  // Will NOT be registered at /defaultPrefix/user
  // WILL be registered at /user
  fastify.get('/', async () => 'Routes/User')
  return true;
}

export const autoPrefix = '/user';
```

## Auto Prefixing

For the given file structure

<pre>
server.js
└── routes/
    ├── path1/
    │   ├── file1.js
    │   └── file2.js
    └── path2/
        ├── file3.js
        └── file4.js
</pre>

If we do the following

```js
fastify.register(Autoload, {
  dir: resolve(process.cwd(), 'routes');
});
```

The routes added in `file1.js` and `file2.js` will have `path1/` prefixed automatically.

Similarly, the routes in `file3.js` and `file4.js` will have their routes prefixed with `path2`.

**NOTE** _This only goes one level deep_. It will not work with any folders inside of `path1/` or `path2/`

## CommonJS Interoperability

The plugin has been tested to work with `.js` files in a folder with `{"type":"commonjs"}` using `module.exports = ...` in place of `export default ...`, so if for some reason you want to use this plugin within a CJS environment, you shouldn't run into any errors.

## Type Definitions

This package comes with it's own TypeScript definitions; no `@types/` repo necessary!
