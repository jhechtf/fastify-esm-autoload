<!-- Please add "[Bug]" (no quotes) to the beginning of your title issue in Gitlab -->
## Info

*ESM Autoload Version:* 

*Fastify Version:*

*Using Fastify Plugin?* 

## Behavior

<!-- Here you can talk about the Expected behavior versus the shown / observed behavior. -->

### Expected

<!-- What would you expect to happen? -->

### Shown

<!-- What is actually happening? -->

## Work Arounds?

<!-- Can you think of a way to get to the expected behavior that might seem a little hacky? -->