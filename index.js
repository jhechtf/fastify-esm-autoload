import { promises } from 'fs';
const { readdir } = promises;
import { resolve, basename } from 'path';
import { pathToFileURL } from 'url';

const fileMatch = /\.m?js$/;
/**
 * 
 * @param {string} dir the directory to read through
 * @param {string|RegExp} ignorePattern ignore files
 * @param {any[]} arr array of files found.
 * @param {number} cur_level the current level we've gone done. Not for direct use.
 */
async function loadDir(dir, ignorePattern = '', arr = [], cur_level = 0) {
  if (cur_level == 2) {
    console.log('nope!');
    return [];
  }
  if (cur_level <= 1) {
    try {
      // read through the directory, including file types
      for (let file of await readdir(dir, { withFileTypes: true })) {
        // if we are a file, and the file matches our test (and does not match the ignore pattern);
        if (file.isFile() && fileMatch.test(file.name) && !(ignorePattern != '' && !file.name.match(ignorePattern))) {
          // push the file into our array
          arr.push({
            name: file.name,
            path: pathToFileURL(resolve(dir, file.name)).href,
            // If we are on the non-base level, include a prefix to be used later.
            prefix: cur_level != 0 ? basename(dir) : ''
          });
        }

        // If we are a directory and our current level is the base, we can go one deeper
        if (file.isDirectory() && cur_level == 0) {
          // wait for the loading of this directory
          // sidenote, this populates the array from the parent call, because Javascript.
          await loadDir(resolve(dir, file.name), ignorePattern, arr, cur_level + 1);
        }
      }
      // After all of that, return the array;
      return arr;

    } catch (e) {
      console.error('Fastify ESM Autoload: Error occurred when trying to read ' + dir);
      console.error(e);
      // return an empty array instead of the arr variable.
      return [];
    }
  }
  return arr;
}

export default async function Autoload(fastify, { dir, ignorePattern, options = {}, prefix = '' }, next) {
  if (!dir) {
    next('Autoload must be supplied a directory');
    return false;
  }

  const files = await loadDir(dir, ignorePattern);
  for (let file of files) {
    const { autoPrefix, default: def } = await import(file.path);
    const opts = { ...options, prefix: prefix };
    if (autoPrefix) {
      opts.prefix = autoPrefix;
    }

    if (file.prefix) {
      opts.prefix = [opts.prefix, file.prefix].join('/');
    }

    fastify.register(def, opts);

  }
  next();
}

// No encapsulation for this plugin;
Autoload[Symbol.for('skip-override')] = true;
