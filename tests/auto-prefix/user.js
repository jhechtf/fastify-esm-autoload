export default async (fastify) => {
  fastify.get('/', async () => 'User Index');
  return true;
};

export const autoPrefix = '/user';