import test from 'ava';
import { resolve } from 'path';
import Fastify from 'fastify';
import Autoload from '../index.js';
import pwd from './pwd.js';

const app = Fastify();

app.register(Autoload, {
  dir: resolve(pwd(import.meta.url), 'interop')
});

test('Works with Common JS', async t => {
  await app.inject({ url: '/interop' })
    .then(res => {
      t.is(res.statusCode, 200);
      t.is(res.payload, 'interop');
    });
});

