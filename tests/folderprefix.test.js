import test from 'ava';
import Fastify from 'fastify';
import { resolve } from 'path';
import Autoload from '../index.js';
import pwd from './pwd.js';

const app = Fastify();

app.register(Autoload, {
  dir: resolve(pwd(import.meta.url), 'folder-prefixing')
});

test('Folder Prefixing Works', async t => {

  await app.inject({ url: '/' })
    .then(res => {
      t.is(res.statusCode, 200, 'direct route works');
    })
    .catch(err => t.fail(err));

  await app.inject({ url: '/something' })
    .then(res => {
      t.is(res.statusCode, 200, 'auto-prefixed routes work');
    })
    .catch(err => t.fail(err));

  await app.inject({ url: '/testing' })
    .then(res => {
      t.is(res.statusCode, 200);
    })
    .catch(err => t.fail(err));
  await app.inject({ url: '/something/nindex' })
    .then(res => {
      t.is(res.statusCode, 200);
    })
    .catch(err => t.fail(err));
  t.is(app.something, 'hello');
});
