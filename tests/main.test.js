import test from 'ava';

import { resolve } from 'path';
import pwd from './pwd.js';
import Fastify from 'fastify';
import Autoload from '../index.js';

const app = Fastify();

const __dir = pwd(import.meta.url);

app.register(Autoload, {
  dir: resolve(__dir, 'mjs')
});

app.register(Autoload, {
  dir: resolve(__dir, 'prefix'),
  prefix: '/prefix'
});

app.register(Autoload, {
  dir: resolve(__dir, 'fp')
});

app.register(Autoload, {
  dir: resolve(__dir, 'blankdir')
});

test('App should load regular JS files', async t => {
  await app.inject({ url: '/' })
    .then(res => {
      t.is(res.statusCode, 200, 'Status Code is good');
      t.deepEqual(JSON.parse(res.payload), { results: [1, 2, 3] }, 'Something Else');
    })
    .catch(err => {
      t.fail('Error occurred: ' + err);
    });
});

test('App should also load .mjs Files', async t => {
  return app.inject({ url: '/route2' })
    .then(res => {
      const parsedBody = JSON.parse(res.payload);
      t.is(res.statusCode, 200, 'MJS Status Code works');
      t.deepEqual(parsedBody, { id: true });
    })
    .catch(err => {
      t.fail(err);
    });
});

test('Check that prefixes are loaded', async t => {
  await app.inject({ url: '/prefix/echo' })
    .then(res => {
      t.is(res.statusCode, 200, '/echo status code correct');
      t.is(res.payload, 'echo', '/echo route works');
    })
    .catch(err => t.fail(err));
});

test('Should work with Fastify Plugin', async t => {
  await app.inject({ url: '/with-plugin' })
    .then(res => {
      t.is(res.statusCode, 200, 'Responds with good error code');
      t.is(res.payload, 'Correct', 'returns correct value');
    })
    .catch(err => t.fail(err));
});


