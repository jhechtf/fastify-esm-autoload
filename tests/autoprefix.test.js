import test from 'ava';
import { resolve, dirname } from 'path';
import { fileURLToPath } from 'url';
import Fastify from 'fastify';
import Autoload from '../index.js';

const fastify = Fastify();

fastify.register(Autoload, {
  dir: resolve(dirname(fileURLToPath(import.meta.url)), 'auto-prefix')
});

fastify.register(Autoload, {
  dir: resolve(dirname(fileURLToPath(import.meta.url)), 'auto-prefix', 'override'),
  prefix: '/defaultPrefix'
});

test('Auto Prefix Working', async t => {
  await fastify.inject({ url: '/user' })
    .then(res => {
      t.is(res.statusCode, 200, 'Status Code is OK');
      t.is(res.payload, 'User Index');
    })
    .catch(err => {
      t.fail(err.toString());
    });

  await fastify.inject({ url: '/login', method: 'POST' })
    .then(res => {
      t.is(res.statusCode, 200, 'POST status code works');
      t.is(res.payload, 'Posted Login');
    })
    .catch(err => {
      t.fail(err);
    });
  return true;
});

test('Auto Prefix Correctly Overrides default', async t => {
  fastify.get('/', async () => fastify.printRoutes());
  await fastify.inject({ url: '/sub/user' })
    .then(res => {
      t.is(res.statusCode, 200);
    })
    .catch(err => {
      t.fail(err);
    });
  return true;
});