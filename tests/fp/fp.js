import fp from 'fastify-plugin';

export default fp(async (fastify) => {
  fastify.get('/with-plugin', async () => 'Correct');
  return true;
});