import { dirname } from 'path';
import { fileURLToPath } from 'url';
/**
 * 
 * @param {string} fileUrl a file URL
 * @returns string
 * @description return the directory name of the given fileUrl
 */
export default function pwd(fileUrl) {
  return dirname(fileURLToPath(fileUrl));
}