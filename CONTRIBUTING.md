## Commits

Commits should follow the syntax of 

```
<type>(<scope?>): Summarization

Explanation of work; what was fixed or changed, why

Ticket References
```

`<scope>` is optional, and is meant to narrow down the section of the code the commit pertains to. 

`<type> is one of:

* build - Changes that affect the build system or external dependencies
* ci - Changes to the CI configuration files / scripts
* docs - Documentation only changes
* feat - New Feature
* fix - Bug Fix
* perf - Changes made make code more performant
* style - Changes that do not affect the meaning of code, only how it is formatted.
* test - Pertaining to testing setup.

commit messages should be written in the past tense -- especially for bug / features as they will become a part of the changelog later on.

Any commits not following this style will not be merged.

Examples commits

```
feat: Added prefixOverride option

Closes #5
```