import { Plugin, RegisterOptions } from 'fastify';
import { Server, IncomingMessage, ServerResponse } from 'http';
interface PluginOptions {
  dir: string;
  ignorePattern?: RegExp | string;
  options?: RegisterOptions<Server, IncomingMessage, ServerResponse>;
}
declare const fastifyAutoloadEsm: Plugin<Server, IncomingMessage, ServerResponse, PluginOptions>;
export = fastifyAutoloadEsm;